package com.wlsendia.sugang.model;

import com.wlsendia.sugang.enums.LectureName;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SugangRequest {

    @NotNull
    @Length(min = 4, max = 28)
    private String accountName;

    @NotNull
    @Length(min = 2, max = 22)
    private LectureName lectureName;
}
