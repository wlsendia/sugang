package com.wlsendia.sugang.model;

import com.wlsendia.sugang.enums.DepartmentName;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StudentRegistrationRequest {
    @NotNull
    @Length(min = 4, max = 28)
    private String accountName;

    @NotNull
    @Min(0)
    @Max(6)
    private Integer grade;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DepartmentName studentDepartment;
}
