package com.wlsendia.sugang.controller;

import com.wlsendia.sugang.model.SugangRequest;
import com.wlsendia.sugang.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/student")
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/set-sugang")
    public String setSugang(@RequestBody @Valid SugangRequest request){
        studentService.setSugang(request);

        return "1";
    }
    @PutMapping("/del-sugang")
    public String delSugang(@RequestBody @Valid SugangRequest request){
        studentService.delSugang(request);

        return "2";
    }
}
