package com.wlsendia.sugang.controller;

import com.wlsendia.sugang.model.StudentRegistrationRequest;
import com.wlsendia.sugang.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/manager")
public class ManagerController {
    private final ManagerService managerService;

    @PostMapping("/set-student")
    public String setStudent(@RequestBody @Valid StudentRegistrationRequest request){
        managerService.setStudent(request);

        return "setStudent";
    }
}
