package com.wlsendia.sugang.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DepartmentName {
    COM("컴퓨터공학과"),
    IEE("정보전기전자");



    private final String name;
}
