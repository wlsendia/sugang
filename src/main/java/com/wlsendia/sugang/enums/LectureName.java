package com.wlsendia.sugang.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LectureName {
    CLANG("C언어", 0, "컴퓨터공학과"),
    PYTHON1("파이썬 1학년", 1, ""),
    PYTHON2("파이썬 2학년",2, "컴퓨터공학과"),
    JAVA("자바",0, ""),
    LINUX("리눅스 3학년",3, "컴퓨터공학과");

    private final String name;

    private final Integer grade;

    private final String department;

}
