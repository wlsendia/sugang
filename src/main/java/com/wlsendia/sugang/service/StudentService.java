package com.wlsendia.sugang.service;

import com.wlsendia.sugang.entity.HistoryEntity;
import com.wlsendia.sugang.entity.StudentEntity;
import com.wlsendia.sugang.model.SugangRequest;
import com.wlsendia.sugang.repository.HistoryEntityRepository;
import com.wlsendia.sugang.repository.StudentEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentEntityRepository studentRepo;
    private final HistoryEntityRepository historyRepo;

    public void setSugang(SugangRequest request) {//계정명, 강좌명
//        StudentEntity studentId = studentRepo.findIdByAccountName(request.getAccountName());//키 값
//        HistoryEntity addData = new HistoryEntity.HistoryEntityBuilder(
//                studentRepo.findIdByAccountName(request.getAccountName()),
//                request.getLectureName())
//                .build();

        historyRepo.save(new HistoryEntity.HistoryEntityBuilder(
                        studentRepo.findIdByAccountName(request.getAccountName()),
                        request.getLectureName())
                        .build()
        );
    }
    public void delSugang(SugangRequest request) {//계정명, 강좌명
        StudentEntity studentId = studentRepo.findIdByAccountName(request.getAccountName());
        HistoryEntity originData = historyRepo.findByStudentEntityAndLectureName(studentId, request.getLectureName());
        originData.putHistory2Delete();

    }
}
