package com.wlsendia.sugang.service;


import com.wlsendia.sugang.entity.StudentEntity;
import com.wlsendia.sugang.model.StudentRegistrationRequest;
import com.wlsendia.sugang.repository.StudentEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final StudentEntityRepository studentRepo;

    public void setStudent(StudentRegistrationRequest request){
        studentRepo.save(new StudentEntity.StudentEntityBuilder(request).build());
    }
}
