package com.wlsendia.sugang.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
