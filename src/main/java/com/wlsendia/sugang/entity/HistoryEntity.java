package com.wlsendia.sugang.entity;

import com.wlsendia.sugang.enums.LectureName;
import com.wlsendia.sugang.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "studentEntityId", nullable = false)
    private StudentEntity studentEntity;

    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private LectureName lectureName;

    @Column(nullable = false)
    private Boolean isTrue;

    private HistoryEntity(HistoryEntityBuilder builder){
        this.studentEntity = builder.studentEntity;
        this.lectureName = builder.lectureName;
        this.isTrue = builder.isTrue;
    }

    public static class HistoryEntityBuilder implements CommonModelBuilder<HistoryEntity> {
        private final StudentEntity studentEntity;
        private final LectureName lectureName;
        private final Boolean isTrue;

        public HistoryEntityBuilder(StudentEntity studentId, LectureName lectureName){
            this.studentEntity = studentId;
            this.lectureName = lectureName;
            this.isTrue = true;
        }

        @Override
        public HistoryEntity build() {
            return new HistoryEntity(this);
        }
    }

    public void putHistory2Delete(){
        isTrue = false;
    }
}
