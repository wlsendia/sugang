package com.wlsendia.sugang.entity;

import com.wlsendia.sugang.enums.DepartmentName;
import com.wlsendia.sugang.interfaces.CommonModelBuilder;
import com.wlsendia.sugang.model.StudentRegistrationRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Length(min = 4, max = 30)
    private String accountName;

    @Column(nullable = false)
    private Integer grade;

    @Column(nullable = false, length = 25)
    @Enumerated(value = EnumType.STRING)
    private DepartmentName studentDepartment;
    //처음에 스트링이라고 지정을 안 해줬더니 인티저형으로 만들어져버림

//    private DepartmentName studentDepart2ment;
//
//    @Length(max = 12)
//    private DepartmentName studentDepart3ment;
//
//    @NotNull
//    private DepartmentName studentDepart4ment;
//
//    @NotNull
//    @Length(max = 21)
//    private DepartmentName studentDepart5ment;
    // -> 4가지 객체 모두 다 인티저형으로 만들어짐, 오류생김

    private StudentEntity(StudentEntityBuilder builder) {
        this.accountName = builder.accountName;
        this.grade = builder.grade;
        this.studentDepartment = builder.studentDepartment;
    }

    public static class StudentEntityBuilder implements CommonModelBuilder<StudentEntity> {

        private final String accountName;
        private final Integer grade;
        private final DepartmentName studentDepartment;

        public StudentEntityBuilder(StudentRegistrationRequest request) {
            this.accountName = request.getAccountName();
            this.grade = request.getGrade();
            this.studentDepartment = request.getStudentDepartment();

        }


        @Override
        public StudentEntity build() {
            return new StudentEntity(this);
        }
    }

}
