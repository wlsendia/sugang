package com.wlsendia.sugang.repository;

import com.wlsendia.sugang.entity.HistoryEntity;
import com.wlsendia.sugang.entity.StudentEntity;
import com.wlsendia.sugang.enums.LectureName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoryEntityRepository extends JpaRepository<HistoryEntity, Long> {
    HistoryEntity findByStudentEntityAndLectureName(StudentEntity studentEntity, LectureName lectureName);
}
