package com.wlsendia.sugang.repository;

import com.wlsendia.sugang.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentEntityRepository extends JpaRepository<StudentEntity, Long> {
    StudentEntity findIdByAccountName(String accountName);
}
